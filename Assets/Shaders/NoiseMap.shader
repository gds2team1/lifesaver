﻿Shader "Unlit/NoiseMap"
{
	Properties
	{
		_ShaderType("Shader type", int) = 0

		[Header(Common Settings)]
		_Scale("Scale", float) = 1.0
		_WaveHeight ("Wave Height", float) = 2.0
		_NumWaves("Number of Waves", int) = 5
		_WaveSpeed ("Wave Speed", float) = 20.0
		_Wavelength ("Wave Length", float) = 0.2


		[Header(Settings for type 1 only)]
		_WaveDir ("Wave Direction", float) = 90.0
		_Phaseshift ("Wave Direction Change", float) = 28.01
		_WaveNoise ("Wave Noise", float) = 1.0
		_WaveHeightChange ("Wave Height Change", float) = 0.6
		_WaveLengthChange ("Wave Length Change", float) = 1.0
		_WaveSpeedChange ("Wave Speed Change", float) = 1.1
		_WaveClampMax("Wave Height Clamp Max", float) = 10.0 
		_WaveClampMin("Wave Height Clamp Min", float) = -10.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
		


			float hash(float2 p) {
			  return frac(sin(dot(p, float2(43.232, 75.876)))*4526.3257);   
			}

			float2 hash2(float2 p ) {
			   return frac(sin(float2(dot(p, float2(123.4, 748.6)), dot(p, float2(547.3, 659.3))))*5232.85324);   
			}

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };



            v2f vert (appdata_base v)
            {
                v2f o;
				float4 wpos = v.vertex;
				//o.vertex = float4((v.texcoord.xy-0.5)*2.0, 1.0, 1.0);
				o.vertex = mul(UNITY_MATRIX_MVP, (wpos*2.0));
                o.uv = v.texcoord;
                return o;
            }

            float _Scale;
            float _WaveHeight;
            float _Wavelength;
            float _Phaseshift;
            float _WaveSpeed;

            float _WaveNoise;
			float _WaveHeightChange;
			float _WaveLengthChange;
			float _WaveSpeedChange;
			float _WaveDir;

			int _NumWaves;
			float _WaveClampMax; 
			float _WaveClampMin;

			int _ShaderType;

            float voronoi(float2 p) {
			    float2 n = floor(p);
			    float2 f = frac(p);
			    float md = 5.0;
			    for (int i = -1;i<=1;i++) {
			        for (int j = -1;j<=1;j++) {
			            float2 g = float2(i, j);
			            float2 o = hash2(n+g);
			            o = _Wavelength+_Wavelength*sin((_Time[0]*_WaveSpeed)+5.038*o);
			            float2 r = g + o - f;
			            float d = dot(r, r);
			            if (d<md) {
			              md = d;
			            }
			        }
			    }
			    return  md;
			}

			float3 ov(float2 p) {
			    float3 v =float3(0.0,0.0,0.0);
			    float a = _WaveHeight;
			    for (int i = 0;i<_NumWaves;i++) {
			        v += voronoi(p)*a;
			        p*=2.0;
			        a*=0.5;
			    }
			    return v;

			}

			float4 type1(float2 uv){
            	float amp = _WaveHeight;
				float wLen = _Wavelength;
				float wSpd = _WaveSpeed;

				float finalWaveHeight = 0.0;

				float degToRad = radians(_WaveDir);

				for(int i=0; i<_NumWaves; i++){
					float wlenPied = 2*3.1416/wLen;
					float2 wDir=normalize(float2(cos(degToRad),sin(degToRad)));
					float dotDir = dot(uv, normalize(wDir));
					float noise = (hash(uv) * _WaveNoise);
					finalWaveHeight += (amp+noise)*sin(wlenPied*dotDir + (_Time*wSpd));

					amp *= _WaveHeightChange;
					wSpd *= _WaveSpeedChange;
					wLen *= _WaveLengthChange;
					degToRad += radians(_Phaseshift);

				}
									
				finalWaveHeight = (finalWaveHeight - -_WaveClampMax) / (_WaveClampMax - -_WaveClampMax); 

                return float4(finalWaveHeight,finalWaveHeight,finalWaveHeight,1.0) ;
            }


            float4 type0(float2 uv){
            	float l = smoothstep(0.0, 0.5, ov(uv*5));
			    float4 a = float4(0.0, 0.0, 0.0, 1.0);
			    float4 b = float4(1.0, 1.0, 1.0, 1.0);
                return float4(lerp(a,b,l)) ;
            }


            fixed4 frag (v2f i) : SV_Target
            { 
            	float2 uv = i.uv*_Scale;
            	float4 colour = float4(0,0,0,0);
            	if(_ShaderType == 0)
            		colour = type0(uv);
            	else if(_ShaderType == 1)
            		colour = type1(uv);

            	return colour;
            }
            ENDCG
        }
    }
}