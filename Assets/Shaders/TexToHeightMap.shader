﻿Shader "Unlit/TexToHeightMap"
{
	Properties
	{
		_MainTex ("Noise Map", 2D) = "white" {}
		_WaterCol("Water Colour", Color) = (0.3,0.6,0.9,1.0)
		_LineThickness ("Wake Thickness", float) = 0.03
        _LineColour("Wake Colour", color) = (0,0,0,1)
		_ShadowMultiplier("Shadow Color Multilier", float) = 0.9
        _ShadowPosition("Shadow Position", float) = 0.001
	}
	SubShader
	{

	    Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			Cull Front
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            float _LineThickness;
            sampler2D _MainTex;

            v2f vert (appdata_base v)
            {
            	float4 col = tex2Dlod(_MainTex, float4(v.texcoord.xy, 0.0,0.0));
                float4 wpos = v.vertex;
                wpos.y += col.x*0.04;


			    v2f o;

                //_WorldSpaceLightPos0
                float4 original = mul (UNITY_MATRIX_MVP, wpos);
                float4 normal = normalize(mul(UNITY_MATRIX_MVP, v.normal));

                o.vertex = original + (mul(_LineThickness, normal));
                return o;
            }

			float4 _LineColour;
            
            fixed4 frag (v2f i) : SV_Target
            {
				return _LineColour;
            }
            ENDCG
        	
        }

		Pass
		{
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

			static const float2 size = float2(1.0,0.0);
			static const float3 off = float3(-(1.0/100),0,1.0/100);

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 normal : TEXCOORD1;
			};

			sampler2D _MainTex;

			v2f vert (appdata_base v)
			{
				
				float4 wpos = v.vertex;

				float4 col = tex2Dlod(_MainTex, float4(v.texcoord.xy, 0.0,0.0));

				float s11 = col.x;
			    float s01 = tex2Dlod(_MainTex, float4(v.texcoord.xy+off.xy, 0.0,0.0)).x;
			    float s21 = tex2Dlod(_MainTex, float4(v.texcoord.xy+off.zy, 0.0,0.0)).x;
			    float s10 = tex2Dlod(_MainTex, float4(v.texcoord.xy+off.yx, 0.0,0.0)).x;
			    float s12 = tex2Dlod(_MainTex, float4(v.texcoord.xy+off.yz, 0.0,0.0)).x;

			    float3 va = normalize(float3(size.xy,s21-s01));
			    float3 vb = normalize(float3(size.yx,s12-s10));

			    float3 bump = cross(va,vb);

			    v2f o;
			    wpos.y += s11*0.04;
				o.vertex = mul(UNITY_MATRIX_MVP, wpos);
				o.uv = v.texcoord;


				o.normal = normalize( mul(_World2Object,float4(bump.rgb,1.0))  ) ;

				return o;
			}

			float _ShadowMultiplier;
            float _ShadowPosition;

            float4 _WaterCol;
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);

				float4 colour = _WaterCol;

				float3 light = normalize(_WorldSpaceLightPos0);

                float lightDot  = dot( i.normal.rgb, light);

                if(lightDot  < 0)
			        lightDot  = 0;

			    if (lightDot  > _ShadowPosition){
                	colour*=_ShadowMultiplier;
                	//if(lightDot < (_ShadowPosition+(_ShadowPosition*0.01)))
                		//colour = float4(0.9,0.9,0.9,1.0);
                }


				//return col;
				return colour;
			}
			ENDCG
		}
	}
}
