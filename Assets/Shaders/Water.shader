﻿Shader "Unlit/Water"
{
    Properties
    {
		_Phaseshift ("Phase Shift", float) = 90.0
		_YAmplitude ("Y Amplitude", float) = 10.0
		_XAmplitude ("X Amplitude", float) = 10.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
        LOD 100

        Pass
        {
        	Cull Back
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };


            float _Phaseshift;
			float _YAmplitude;
			float _XAmplitude;

			float rand(float2 myVector)  {
             return frac(sin( dot(myVector ,float2(12.9898,78.233) )) * 43758.5453);
         	}


            v2f vert (appdata_base v)
            {
                v2f o;
				
				float4 wpos =  v.vertex;

				float phase = (_Time*50) ;

				float2 wave = float2(0.5,0.5);
				float2 waveDir = (wpos.xz)* normalize(float2(1,2));

				//wave.x += sin( ( phase ) + ((waveDir.x+waveDir.y)*20) ) * _YAmplitude ;
				//wpos.y += (sin( ( phase ) + ((wpos.z+wpos.x)*40) ) * _YAmplitude)+(sin( ( phase*0.2 ) + ((wpos.z)*20) ) * _YAmplitude);
				//wpos.z += (cos( phase + ((wpos.z+wpos.x)*20) ) * _XAmplitude)+ (cos( (phase*0.1) + ((wpos.z)*20) ) * _XAmplitude);

				//for cresting wave
				//wpos.y += sin(phase + (wpos.z*20)) * _YAmplitude;
				//wpos.y += cos(_Phaseshift + (wpos.y*2)) * _XAmplitude;


				//wpos.x = wave.y;
				//wpos.z = wave.y;

				wpos.y += (sin( ( phase ) + ((wpos.z)*20) ) * _YAmplitude);
				wpos.z += (cos( phase + ((wpos.z)*20) ) * _XAmplitude);

				o.vertex = mul(UNITY_MATRIX_MVP, wpos);
                o.uv = v.texcoord;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            { 
                return float4(1,1,1,1) ;
            }
            ENDCG
        }
    }
}
