﻿Shader "Unlit/Dogs"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _LineThickness ("Line Thickness", float) = 0.03
        _LineColour("Line Colour", color) = (0,0,0,1)
        _ShadowMultiplier("Shadow Color Multilier", float) = 0.9
        _ShadowPosition("Shadow Position", float) = 0.001

        _SpecColor ("Specular Material Color", Color) = (1,1,1,1) 
      	_Shininess ("Shininess", Float) = 10


    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
        LOD 100

        Pass
        {
			Cull Front
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            float _LineThickness;


            v2f vert (appdata_base v)
            {
                v2f o;
                //_WorldSpaceLightPos0
                float4 original = mul (UNITY_MATRIX_MVP, v.vertex);
                float4 normal = normalize(mul(UNITY_MATRIX_MVP, v.normal));

                o.vertex = original + (mul(_LineThickness, normal));
                return o;
            }

			float4 _LineColour;
            
            fixed4 frag (v2f i) : SV_Target
            {
				return _LineColour;
            }
            ENDCG
        	
        }

        Pass
        {
        	Cull Back
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 normal : TEXCOORD1;
                float3 specularReflection : TEXCOORD2;

            };

            uniform float4 _LightColor0;
            float4 _SpecColor;
      		float _Shininess;

            v2f vert (appdata_base v)
            {
                v2f o;
                //_WorldSpaceLightPos0
                o.vertex = mul (UNITY_MATRIX_MVP, v.vertex);
                o.uv = v.texcoord;
                o.normal = normalize(mul(_World2Object, v.normal));
                float3 viewDir = normalize(_WorldSpaceCameraPos - mul(_Object2World, v.vertex).xyz);
            	float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);

            	//Specular
				if (dot(o.normal, lightDir) < 0.0) {
					o.specularReflection = float3(0.0, 0.0, 0.0); 
				}
				else // light source on the right side
				{
					o.specularReflection = _LightColor0.rgb 
					  * _SpecColor.rgb * pow(max(0.0, dot(
					  reflect(-lightDir, o.normal), 
					  viewDir)), _Shininess);
				}

                return o;
            }

            sampler2D _MainTex;
            float _ShadowMultiplier;
            float _ShadowPosition;

            fixed4 frag (v2f i) : SV_Target
            {
            	float4 colour = tex2D(_MainTex, i.uv);
			    colour.a = 1;

	          	float3 light = normalize(_WorldSpaceLightPos0);
                float lightDot  = dot( i.normal, light);

                if(lightDot  < 0)
			        lightDot  = 0;

			    if (lightDot  < _ShadowPosition)
                	colour*=_ShadowMultiplier;

                return float4(colour.rgb+i.specularReflection,1.0) ;
            }
            ENDCG
        }
    }
}