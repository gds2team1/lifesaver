﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class HighScoreController : MonoBehaviour {

	public Text level1;
	public Text level2;
	public Text level3;
	public Text level4;
	public Text level5;

	public GameObject checkPanel;

	void Start () 
	{	
		level1.text = "Level 1:  " + PlayerPrefs.GetInt("Level1", 0).ToString();
		level2.text = "Level 2:  " + PlayerPrefs.GetInt("Level2", 0).ToString();
		level3.text = "Level 3:  " + PlayerPrefs.GetInt("Level3", 0).ToString();
		level4.text = "Level 4:  " + PlayerPrefs.GetInt("Level4", 0).ToString();
		level5.text = "Level 5:  " + PlayerPrefs.GetInt("Level5", 0).ToString();

		checkPanel.SetActive(false);
	}
	
	void Update () 
	{
		level1.text = "Level 1:  " + PlayerPrefs.GetInt("Level1", 0).ToString();
		level2.text = "Level 2:  " + PlayerPrefs.GetInt("Level2", 0).ToString();
		level3.text = "Level 3:  " + PlayerPrefs.GetInt("Level3", 0).ToString();
		level4.text = "Level 4:  " + PlayerPrefs.GetInt("Level4", 0).ToString();
		level5.text = "Level 5:  " + PlayerPrefs.GetInt("Level5", 0).ToString();

		if (Input.GetKeyUp("h")) //reset all high scores hack
		{
			PlayerPrefs.SetInt ("Level1", 0);
			PlayerPrefs.SetInt ("Level2", 0);
			PlayerPrefs.SetInt ("Level3", 0);
			PlayerPrefs.SetInt ("Level4", 0);
			PlayerPrefs.SetInt ("Level5", 0);

		}
	}

	public void activateCheckPanel()
	{
		checkPanel.SetActive(true);
	}

	public void yesReset()
	{
		PlayerPrefs.SetInt ("Level1", 0);
		PlayerPrefs.SetInt ("Level2", 0);
		PlayerPrefs.SetInt ("Level3", 0);
		PlayerPrefs.SetInt ("Level4", 0);
		PlayerPrefs.SetInt ("Level5", 0);

		checkPanel.SetActive(false);
	}

	public void noReset()
	{
		checkPanel.SetActive(false);
	}


}


