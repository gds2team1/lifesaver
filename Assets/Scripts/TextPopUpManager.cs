﻿using UnityEngine;
using System.Collections;

public class TextPopUpManager : MonoBehaviour {

	public static TextPopUpManager Instance{get; set;}

	private void Start()
	{
		//Debug.Log("SMASHTAG Admob initialise");		

		if(Instance == null)
		{
			Instance = this;
		}

		DontDestroyOnLoad(gameObject);
	}

	
	// Update is called once per frame
	void Update () {
	
	}

	public void CreateText(string text, Vector3 position, float r, float g, float b)
	{
		var vacantPosition = GetVacantPosition(position);
		var textResource = Resources.Load("TextPopUp") as GameObject;
        var textPopUp = Instantiate(textResource) as GameObject;  

        textPopUp.GetComponent<TextFlash>().SetText(text, vacantPosition, r, g, b);
	}

	private Vector3 GetVacantPosition(Vector3 position)
	{
		GameObject[] popupsInScene = GameObject.FindGameObjectsWithTag("PopUp");

		//No other popups it must be vacant!
		if(popupsInScene.Length == 0)
		{
			return position;
		}

		int textWidth = 50;
		int textHeight = 20;

		Vector3 newPos = position;

		for(int popUpIndex = 0; popUpIndex < popupsInScene.Length; popUpIndex++)
		{
			var otherPosition = popupsInScene[popUpIndex].transform.position;
			//check whether x and y might be overlapping
			bool overlappingY = newPos.y < (otherPosition.y + textHeight) &&  newPos.y > (otherPosition.y - textHeight);
			bool overlappingX = newPos.x < (otherPosition.x + textWidth) &&  newPos.x > (otherPosition.x - textWidth);

			//If overlapping just move the y up
			if(overlappingX && overlappingY)
			{
				newPos = new Vector3(newPos.x, newPos.y + textHeight, newPos.z);
			}

		}

		//No overlaps found just return position
		return newPos;

	}
}
