﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class BubbleAnimator : MonoBehaviour
{

	private Image image;
	public Sprite[] sprites;
	private float fps;
	private int frame;
	private bool pop;
	private bool kill;
	private float deltaTime;

	// Use this for initialization
	void Start ()
	{
		fps = 0.05f;
		frame = 0;
		pop = false;
		kill = false;
		deltaTime = 0f;
		image = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (pop) {
			Animate ();
			if (kill) {
				this.gameObject.SetActive (false);
			}
		}
	}

	public void Pop ()
	{
		pop = true;
	}

	private void Animate()
	{
		bool finished;
		deltaTime += Time.deltaTime;

		while (deltaTime >= fps) {
			deltaTime -= fps;
			frame++;
			if (frame >= sprites.Length) {
				frame = sprites.Length - 1;
				kill = true;
			}
		}
		image.sprite = sprites [frame];
	}
}
