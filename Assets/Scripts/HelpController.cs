﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HelpController : MonoBehaviour {

	//Overlay spirte
	private SpriteRenderer _overlaySprite;
	private float _lastFadeAlpha = 0;
	private bool _isFadingIn = false;
	private float _timeScale = 1.0f;

	public GameObject overlay;
	public GameObject helpGUI;
	public GameObject pauseGUI;
	public GameObject interactableCat;
	public Text speech;
	public GameObject hand;

	private	string _message = "Meow!  Pull  down  on  me  and  release  to  cast  me  into  the  water.  Land  near  any  dogs  and  I  will  drag  them  to  safety!";
	private Vector3 _initialHandPosition;

	private TimerController _timer;


	private float _startTime = 0;
	private bool _isPaused = false;
	private bool _notShown = true;

	// Use this for initialization
	void Start () {
		_overlaySprite = overlay.GetComponent<SpriteRenderer>();

		_startTime = Time.time;
		_initialHandPosition = hand.transform.position;

		_timer = GameObject.Find("Timer").GetComponent<TimerController>();
			
	}
	
	// Update is called once per frame
	void Update () {
	
		float elapsedTime = Time.time - _startTime;

		if(_notShown && elapsedTime > 1)
		{

			needHelp();
			_notShown = false;			
		}

		if(_isFadingIn)
		{
			//Debug.Log("Fade that shit like woh");
			fadeInOverlay();
		}

		if(_isPaused)
		{
			moveHand();
		}

		bool interacted = false;
		if(interacted && _isPaused)
		{
			resume();
		}
	}

	/*Look in editor - Event Trigger component*/
	public void DragStarted() {
        Debug.Log("Dragged");

        //Cursor will be locked until end of message
        resume();
    }

	void moveHand()
	{
		float newY = Mathf.Lerp(hand.transform.position.y, _initialHandPosition.y - 100, _timeScale/40);

		//At bottom reset to start 
		if(newY < (_initialHandPosition.y - 80))
		{
			newY = _initialHandPosition.y;
		}

		hand.transform.position = new Vector3(hand.transform.position.x, newY, hand.transform.position.z);
	}

	void needHelp()
	{
		_timer.StopTimer();

		//Show Overlay
		overlay.SetActive(true);

		_overlaySprite.color = new Color(1f, 1f, 1f, 0.1f);
		_lastFadeAlpha = 0.1f;	
		//Sdhow Paue UI
		pauseGUI.SetActive(false);

		_timeScale = 1;
		Time.timeScale = 0;
		_isFadingIn = true;

		//Dont allow movement until message message finished
		Cursor.visible = false;
     	Cursor.lockState = CursorLockMode.Locked;
	}


	void fadeInOverlay()
	{

		_lastFadeAlpha = Mathf.Lerp(_lastFadeAlpha, 0.9f, _timeScale * 0.1f);
		_overlaySprite.color = new Color(1f, 1f, 1f, _lastFadeAlpha);

		//Debug.Log("Lets make alpha: " + _lastFadeAlpha + " with scale: " + _timeScale);
		//Exit if reached certain Fade - and move fade in
		if(_lastFadeAlpha > 0.8f)
		{
			//Debug.Log("Hit the alpha");
			_isFadingIn = false;
			_isPaused = true;

			showHelp();
		}
	}

	void showHelp()
	{
		//Show score scoreGUI
		StartCoroutine (TypeText ()); 

		helpGUI.SetActive(true);
		interactableCat.SetActive(true);

		//Play the meow
		AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
	}

	void resume()
	{
		removeOverlay();
		Time.timeScale = Constants.timeScale;
	}

	void removeOverlay()
	{
		interactableCat.SetActive(false);		
		//Show score scoreGUI
		helpGUI.SetActive(false);
		//Hide Overlay
		overlay.SetActive(false);
		//Sdhow Paue UI
		pauseGUI.SetActive(true);

		_isPaused = false;
		_timer.StartTimer();


		Destroy(gameObject);

	}

	IEnumerator TypeText()
	{
		float charTime = 0.03f;

		foreach (char letter in _message.ToCharArray()) {
			speech.text += letter;
			yield return StartCoroutine(CoroutineUtil.WaitForRealSeconds(charTime));
		}
		
		//Allow them to interact once message finished
		Cursor.visible = true;
     	Cursor.lockState = CursorLockMode.None;
	}
}

/*Work around class since time is stopped to allow tutorial*/
public static class CoroutineUtil
 {
     public static IEnumerator WaitForRealSeconds(float time)
     {
         float start = Time.realtimeSinceStartup;
         while (Time.realtimeSinceStartup < start + time)
         {
             yield return null;
         }
     }
 }
