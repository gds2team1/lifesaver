﻿public enum GameState
{
	Playing = 1,
	Paused = 2,
	GameOver = 3,
	GameWon = 4
}