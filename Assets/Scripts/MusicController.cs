﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MusicController : MonoBehaviour {

	// Use this for initialization
	public AudioSource backgroundMusic;
	public Toggle toggle;

	void Start () 
	{
		// 0 = on, 1 = off
		if (PlayerPrefs.GetInt ("backgroundMusicMute") == 0) {
			if (toggle != null) {
				toggle.isOn = true;
			}
			backgroundMusic.mute = false;
		} else {
			if (toggle != null) {
				toggle.isOn = false;
			}
			backgroundMusic.mute = true;
		}
		backgroundMusic.ignoreListenerPause = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void toggleMuteMusic()
	{
		if (backgroundMusic.mute) 
		{
			backgroundMusic.mute = false;
			PlayerPrefs.SetInt ("backgroundMusicMute", 0);
		} 
		else 
		{
			backgroundMusic.mute = true;
			PlayerPrefs.SetInt ("backgroundMusicMute", 1);
		}
	}
		
}
