﻿using UnityEngine;
using System.Collections;

public class ReelingState : IDogState{

	private readonly DogController dog;

	private Vector3 lurePreviousPos;

	//The last dog in the conga line or lure itself if no other dogs are attached
	private GameObject moveTowards;

	private float minDist;
	// frame buffer
	private float framesInCatchableZone = 0;

	public ReelingState(DogController dog)
	{
		this.dog = dog;
	}

	public void setMoveTowards(GameObject mt){
		moveTowards = mt;
		minDist = moveTowards.GetComponent<SphereCollider> ().radius*1.1f;
	}

	public void UpdateState ()
	{
		if (moveTowards != null) {
			lurePreviousPos = moveTowards.transform.position;
			//Debug.Log (moveTowards.transform.position);
		}

		Vector3 lurePreviousPosYZeroed = new Vector3 (lurePreviousPos.x, 0.0f, lurePreviousPos.z);

		dog.transform.rotation = Quaternion.Slerp (dog.transform.rotation,
			Quaternion.LookRotation (dog.transform.position - lurePreviousPosYZeroed), Constants.DOG_TURN_SPEED * Time.deltaTime);

		float distance = Vector3.Distance (lurePreviousPosYZeroed, dog.transform.position);

		if (distance > minDist) {
			dog.transform.position += dog.transform.forward * (Constants.DOG_SWIM_SPEED) * dog.lureController.reelSpeed * Time.deltaTime;
		}
			
		// After 1 frame, dog is rescued. This is to prevent a bug where dog would get stuck at z = -7.9xxx and wouldn't be rescued.
		if (dog.transform.position.z <= Constants.platformZ + 0.5f) {
			if (framesInCatchableZone >= 1) {
				dog.Rescued ();
			}
			framesInCatchableZone++;
		}
	}
}
