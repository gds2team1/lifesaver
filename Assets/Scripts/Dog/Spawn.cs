﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

	public static int spawnCount;
	public int spawnLimit;
	public static int drownCount;
	public static int saveCount;
	public float delay;
	public double speedUp;
	public GameObject[] dogPrefabs;
	private GameObject dogs;

	// Use this for initialization
	void Start () {
		spawnCount = 0;
		drownCount = 0;
		saveCount = 0;
		spawnLimit = 5;
		speedUp = 0.02;

		for (int i = 0; i < spawnLimit; i++) {
			SpawnObjects ();
		}
	}

	void Update () {
		if (spawnCount < spawnLimit) {
			Invoke ("SpawnObjects", delay);
		}
	}

	// Use to spawn an instant of the dog prefab
	void SpawnObjects()
	{
		float x = Random.Range (-3f, 3f);
		float z = Random.Range (-7f, -3f);

		Vector3 spawnLocation = new Vector3 (x, 0f, z);
		GameObject dog = dogPrefabs.PickRandom ();
		dogs = Instantiate (dog, spawnLocation, dog.transform.rotation) as GameObject;
		dogs.transform.parent = transform;
		spawnCount++;
		delay -= (float)speedUp;
		//Debug.Log("delay = " + delay);
		CancelInvoke ();
	}
}