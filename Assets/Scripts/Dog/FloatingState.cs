﻿using UnityEngine;
using System.Collections;

public class FloatingState : DogBase, IDogState{

	private readonly DogController dog;
	
	//Floating
	float maxUpAndDown = 0.05f;             // amount of meters going up and down
 	float speed= 200;            // up and down speed
  	float angle = 0;            // angle to determin the height by using the sinus
  	float toDegrees = Mathf.PI/180;  

	public FloatingState(DogController dog) : base(dog)
	{
		this.dog = dog;
	}

	public void UpdateState() 
	{
		//Base class method
		LureSuper();

		Float();
	}

	private void Float()
	{		
		angle += speed * Time.deltaTime;

     	if (angle > 360) 
     	{
     		angle -= 360;
     	}

		dog.transform.position = new Vector3(dog.transform.position.x, maxUpAndDown * Mathf.Sin(angle * toDegrees), dog.transform.position.z);
	}
}
