﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DogController : MonoBehaviour {

	public float gravityRadius = 1.0f;
	public bool canDrown;
	public float timeTillDrown = 2;	
	public float drownTime = 10;	
	private Rigidbody rigidbody;

	public int score = 100;

    public AudioClip drowningSound;
	public AudioClip deathSound;

    //Animator
    private Animator animator;

	//Drown timer
	private Coroutine _drownTimer = null;

	public GameObject lure; 
	[HideInInspector]
	public SlingShotController lureController;

	//Particle Systems
	public GameObject stars;
	public GameObject splashPrefab;
	private GameObject splash;
	public GameObject drownSplashPrefab;
	private GameObject drownSplash;

	//States
	[HideInInspector] public IDogState currentState;
	[HideInInspector] public RisingState risingState;
    [HideInInspector] public FloatingState floatingState;
    [HideInInspector] public ReelingState reelingState;
    [HideInInspector] public DrowningState drowningState;
    [HideInInspector] public DieingState dieingState;

    private GameController _gameInstance;
	private GameState previousGameState;
	private string currentLevel;

    private bool _hasRing = false;


	private void Awake()
    {
		risingState = new RisingState (this);
		floatingState = new FloatingState(this);
        reelingState = new ReelingState(this);
        drowningState = new DrowningState(this);
        dieingState = new DieingState(this);

        currentState = risingState;  
		_gameInstance = GameController.GetInstance();
		currentLevel = SceneManager.GetActiveScene ().name;
    }

	// Use this for initialization
	void Start () 
	{
		previousGameState = GameState.Playing;

		rigidbody = GetComponent<Rigidbody>();
		animator = GetComponent<Animator>();

		lure = GameObject.Find ("Lure");
		if (lure != null) {
			lureController = lure.GetComponent<SlingShotController> ();
		}

		//set gravity radius for different levels to make game harder
		//default = 1 for Level 1
		string currentLevel = SceneManager.GetActiveScene ().name;
		switch (currentLevel)
		{
		case "Level2":
			gravityRadius = 0.7f;
			break;
		case "Level3":
			gravityRadius = 0.5f;
			break;
		case "Level4":
		case "Level5":
			gravityRadius = 0.4f;
			break;
		default:
			gravityRadius = 1f;
			break;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		// Pause the dog sounds while paused and unpause them when resuming
		if (GameController.GetInstance ().GetGameState () == GameState.Paused && previousGameState == GameState.Playing) {
			GetComponent<AudioSource> ().Pause ();
			previousGameState = GameState.Paused;
		} else if (GameController.GetInstance ().GetGameState () == GameState.Playing && previousGameState == GameState.Paused) {
			GetComponent<AudioSource> ().UnPause ();
			previousGameState = GameState.Playing;
		}
	}

	void FixedUpdate()
	{
		if (currentState != null) {
			currentState.UpdateState ();
			if(currentState == risingState)
				CheckHeight ();
		}
	}

	public void CheckHeight()
	{
		//Determine whether the dog has reached the water
		if (risingState.GetHeight() >= -0.05f) {
			currentState = floatingState;
			if (currentLevel != "Level5")
			{
				splash = Instantiate (splashPrefab, this.transform.position, Quaternion.identity) as GameObject;
			}
			_drownTimer = StartCoroutine (DrownTimer());
		}
	}

	public void Caught()
	{
		Debug.Log("Caught!");
		currentState = reelingState;
		if (splash != null) {
			DestroyObject (splash);
		}
		if (drownSplash != null) {
			DestroyObject (drownSplash);
		}

		reelingState.setMoveTowards (lureController.lastInTheConga);
		//If it has a ring on, remove that shit it's reeling time
		if(_hasRing)
		{
			DestroyRing();
		}

		//Change animation state
		animator.SetBool("Reeling", true);

	}

	public void Rescued()
	{
		Instantiate (stars, this.transform.position, Quaternion.identity);

		_gameInstance.DogRescued(score, transform.position);
		_gameInstance.removeFromDogList (gameObject);

		//Change animation state
		Destroy(gameObject);
	}

	public void Drown()
	{
		Debug.Log("Into drown");
		currentState = drowningState;
		if (splash != null) {
			DestroyObject (splash);
		}

		if (currentLevel != "Level5") {
			drownSplash = Instantiate (drownSplashPrefab, this.transform.position, Quaternion.identity) as GameObject;
		}
		//Change animation state		
		animator.SetBool("Drowning", true);
       	GetComponent<AudioSource>().PlayOneShot(drowningSound); //drowning sound
	}

	public void KillTheDog()
	{
		StartCoroutine(Die());
	}

	private IEnumerator Die()
	{
		//Twirl it around and make it sink
		GetComponent<AudioSource> ().PlayOneShot (deathSound);
		rigidbody.constraints = RigidbodyConstraints.None;
		currentState = dieingState;	


		yield return new WaitForSeconds(1.5f);

		_gameInstance.DogDied();

		_gameInstance.removeFromDogList (gameObject);

		//Destroy
		DestroyObject(gameObject);
		if (splash != null) {
			DestroyObject (splash);
		}
		if (drownSplash != null) {
			DestroyObject (drownSplash);
		}
	}

	private IEnumerator DrownTimer()
	{
		yield return new WaitForSeconds (timeTillDrown);


		if(currentState == floatingState && canDrown && !_hasRing)
		{
			Drown();
		}

		yield return new WaitForSeconds (drownTime);

		/*Delay that drowning while it has a ring
		while(_hasRing)
		{
			yield return new WaitForSeconds(3);
		}*/

		if(currentState == drowningState && gameObject != null && !_hasRing)
		{
			Debug.Log("Reached death");
			KillTheDog();
		}

	}

	public void SetRingTimeout()
	{
		_hasRing = true;
		currentState = floatingState;
		//Stop their drowning!
		StopCoroutine(DrownTimer());
		_drownTimer = null;

		StartCoroutine(RemoveRing());
	}

	private IEnumerator RemoveRing()
	{
		float ringTime = 5;

		yield return new WaitForSeconds(ringTime);
		if(_hasRing)
		{
			DestroyRing();

			// Start the drown timer again
			currentState = floatingState;
			_drownTimer = StartCoroutine(DrownTimer());
		}

	}

	private void DestroyRing()
	{
		//Find the ring 

		var ring = transform.Find ("Ring(Clone)");
		Destroy(ring.gameObject);

		//Destroy it

		_hasRing = false;

	}
}