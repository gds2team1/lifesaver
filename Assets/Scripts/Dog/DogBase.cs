﻿using UnityEngine;
using System.Collections;

public class DogBase {

	private readonly DogController dog;

	public DogBase(DogController dog)
	{
		this.dog = dog;
	}

	public void LureSuper()
	{
		if (dog.lure != null) {
			float distanceToBall = Vector3.Distance (dog.lure.transform.position, dog.transform.position);
			if (distanceToBall < dog.gravityRadius) {
				//Vector3 directionToBall = lure.transform.position - transform.position;
				//directionToBall.Normalize();
				// rigidbody.AddForce (directionToBall * Constants.gravityStrength);

				//Strange effects when above was combined with reel ^
				//dog.transform.position = Vector3.MoveTowards(dog.transform.position, dog.lure.transform.position, Time.deltaTime);
				dog.Caught ();
				dog.lureController.dogCaught (dog.gameObject);

			}
		}
	}
}
