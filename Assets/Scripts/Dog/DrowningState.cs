﻿using UnityEngine;
using System.Collections;

public class DrowningState : DogBase, IDogState{

	private readonly DogController dog;

  	//shake
  	float shakeAngle = 0;            // angle to determin the height by using the sinus
  	float maxShake = 10;            // angle to determin the height by using the sinus
  	float toDegrees = Mathf.PI/180;  
 	float speed= 1000;            // up and down speed



	public DrowningState(DogController dog) : base(dog)
	{
		this.dog = dog;
	}

	public void UpdateState() 
	{
		Drown();

		LureSuper();
	}

	private void Drown()
	{
		shakeAngle += speed * Time.deltaTime;

     	if (shakeAngle > 360) 
     	{
     		shakeAngle -= 360;
     	}

		//Shaking mechanism
		dog.transform.eulerAngles = new Vector3(0, maxShake * Mathf.Sin(shakeAngle * toDegrees), 0);
	}
}
