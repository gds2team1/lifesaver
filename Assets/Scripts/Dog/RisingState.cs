﻿using UnityEngine;
using System.Collections;

public class RisingState : DogBase, IDogState{

	private readonly DogController dog;

	float riseRate = 0.05f;   // how fast the dog falls down
	float height;	//current y value of

	public RisingState(DogController dog) : base(dog)
	{
		this.dog = dog;
	}

	public void UpdateState() 
	{
		//Base class method
		Rising();
	}

	private void Rising()
	{		
		height = dog.transform.position.y + riseRate;

		dog.transform.position = new Vector3(dog.transform.position.x, height, dog.transform.position.z);
	}

	public float GetHeight()
	{
		return height;
	}
}
