﻿using UnityEngine;
using System.Collections;

public class DieingState : DogBase, IDogState {

	private readonly DogController dog;

	private float downSpeed = 0.005f;
	private float drownSpeed = 150.0f;

	public DieingState(DogController dog) : base(dog)
	{
		this.dog = dog;
	}

	public void UpdateState() 
	{

		//Move further into water
		dog.transform.position = new Vector3(dog.transform.position.x, dog.transform.position.y - downSpeed, dog.transform.position.z);

				//Spiral down into water
		dog.transform.Rotate(0, drownSpeed * Time.deltaTime, 0);

	}
}
