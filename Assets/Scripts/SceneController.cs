﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour 
{
	private bool _isLoading = false;

	public void LoadScene(string scene)
	{
		//Only allow them to click once
		if(!_isLoading){
			StartCoroutine(SoundAndLoad(scene));
		}

	}

	//Coroutine to fade and then load the level
	IEnumerator SoundAndLoad(string scene)
	{
		Time.timeScale = Constants.timeScale;

		_isLoading = true;

		//Can place any sounds or what not here
 
 		//Wait for sound to finish
        float fadeTime = GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime); 
 
        SceneManager.LoadScene(scene);

    }

    public void restartLevel()
	{
		//SceneManager.LoadScene("Main");
		StartCoroutine(LoadBuildIndex(SceneManager.GetActiveScene().buildIndex));
	}

	// Relies on level to be in the right buld order > Build Settings
	public void Nextlevel()
	{
		int index = SceneManager.GetActiveScene().buildIndex;
		index++;
		StartCoroutine(LoadBuildIndex(index));
	}

	//Coroutine to fade and then load the level
	IEnumerator LoadBuildIndex(int scene)
	{
		Time.timeScale = Constants.timeScale;
		_isLoading = true;

		//Can place any sounds or what not here
 
 		//Wait for sound to finish
        float fadeTime = GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime); 
 
		SceneManager.LoadScene(sceneBuildIndex:scene);

    }

    /*BELOW METHODS SHOULD BE DEPRECATED*/

    public void StartGame () 
	{
		Debug.Log("Deprecated - use LoadScene()");
		SceneManager.LoadScene("Level1");
	}

	public void LevelSelect()
	{
		Debug.Log("Deprecated - use LoadScene()");

		SceneManager.LoadScene ("LevelSelect");
	}

	public void LoadSelectedLevel()
	{
		Debug.Log("Deprecated - use LoadScene()");

		Debug.Log (this.gameObject.tag);
		SceneManager.LoadScene (this.gameObject.tag);
	}

	public void loadSettingsMenu () 
	{
		Debug.Log("Deprecated - use LoadScene()");

        SceneManager.LoadScene("Settings");
	}

	public void loadHighScores () 
	{
		Debug.Log("Deprecated - use LoadScene()");

		SceneManager.LoadScene("HighScores");
	}

	public void loadTitleScreen () 
	{
		Debug.Log("Deprecated - use LoadScene()");

        SceneManager.LoadScene("TitleScreen");
	}

	public void LoadCredits()
	{
		Debug.Log("Deprecated - use LoadScene()");
		
		SceneManager.LoadScene("Credits");

	}

	public void QuitGame()
	{
		Application.Quit();
	}
	/*
	public void loadnextLevel () 
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	} */
}
