﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;


//Keep monobehaviour so reinstated each scene - dont worry about leaks
public class GameController : MonoBehaviour {

	private static GameController _instance;
	private List<IGameObserver> _observers = new List<IGameObserver>();

    public int highScore = 0;
    //public string highScoreKey = "HighScore";
	Scene scene;

	private int _score = 0;
	private int _dogsSaved = 0;
	private int _dogsDrowned = 0;
	private int _lives = 3;
	private bool _gameOver = false;
	private int _scoreMultiplier = 1;

	public float spawnItemInterval = 8;

	public GameState _currentGameState;
	private TimerController _timer;
	public GameObject ac;

	private List<GameObject> _dogsInScene = new List<GameObject>();

	[Header("Spawn")]
	public GameObject spawnChecker;
	//The Distance away from the camera edge the dogs should be spawned
	public float sideBuffer = 0.5f;
	public int spawnAtStart;
	public int spawnLimit;

	public float delay;
	public float speedUp;
	private float lastSpawnTime;

	public GameObject[] dogPrefabs;
	public GameObject[] obstacles;

	private Plane[] _camFPlanes;

	private PickUpFactory pickUpFactory;

	public static GameController GetInstance()
	{
		return _instance;
	}

	private void Awake()
	{
		if(_instance != null && _instance != null)
		{
			Destroy(this.gameObject);
		}
		else
		{
			Time.timeScale = Constants.timeScale;
			_instance = this;
		}
	}

	void Start () {
		_currentGameState = GameState.Playing;
		_camFPlanes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

		//no pickups in level 1
		if (SceneManager.GetActiveScene ().name != "Level1") 
		{
			//TODO - pass the level in
			pickUpFactory = new PickUpFactory (0);
			StartCoroutine (SpawnPickUp ());
		}

		float attempts = 0;
		while (_dogsInScene.Count < spawnAtStart && attempts < spawnLimit * 3) {
			SpawnObjects ();
			attempts++;
		}

		ac = GameObject.Find ("Analytics");

		scene = SceneManager.GetActiveScene ();
		Debug.Log ("Current scene name: " + scene.name);
		highScore = PlayerPrefs.GetInt(scene.name, 0); //get highscore for current scene
		// highScore = PlayerPrefs.GetInt(highScoreKey, 0);
    }

	void Update () {
		if (_currentGameState != GameState.GameOver) {
			lastSpawnTime += Time.deltaTime;
			if (_dogsInScene.Count < spawnLimit && lastSpawnTime > delay) {
				if (SpawnObjects ()) {
					lastSpawnTime = 0;
					if (delay > 1) {
						delay -= (float)speedUp;
					}
				} else {
					//Debug.Log ("Spawn skiped");
				}
			}
		}
			
		//Simulating pickups
		if(Input.GetKeyDown("a"))
		{
			TimeBonus();
		}

		if(Input.GetKeyDown("s"))
		{
			StartScoreMultiplier();
		}

        if (Input.GetKeyDown("h")) //reset high score
        {
			
			highScore = 0;
			PlayerPrefs.SetInt(scene.name, highScore);
        }
		if (Input.GetKeyDown("f")) //reset high score
		{

			RubberRingPickedUp ();
		}

		// Destroy all the popups when pausing and finishing game
		if (_currentGameState != GameState.Playing) {
			GameObject[] popups = GameObject.FindGameObjectsWithTag ("PopUp");
			if (popups != null) {
				foreach (GameObject popup in popups) {
					Destroy (popup.gameObject);
				}
			}
		}
			
    }

	public void AddObserver(IGameObserver observer)
	{
		_observers.Add(observer);
	}

	private void NotifyObservers()
	{
		foreach(IGameObserver observer in _observers)
		{
			observer.UpdateGameStats();
		}
	}

	//Add score from rescued dog and notifyObservers
	public void DogRescued(int score, Vector3 dogPosition)
	{
		_dogsSaved += 1;
		IncreaseScore(score);

		//Show text popup
        var position = Camera.main.WorldToScreenPoint(dogPosition);
		TextPopUpManager.Instance.CreateText("+" + score*_scoreMultiplier, position, 1f, 1f, 1f);

		NotifyObservers();
	}

	public void PauseGame()
	{
		Time.timeScale = 0;
		_timer.StopTimer();
		_currentGameState = GameState.Paused;
		NotifyObservers();

	}

	public void ResumeGame()
	{
		Time.timeScale = Constants.timeScale;
		_timer.ResumeTimer();
		_currentGameState = GameState.Playing;
		//Resume generation
		NotifyObservers();

	}

	private void IncreaseScore(int score)
	{
		_score += (score * _scoreMultiplier);

	}

	//Dog died so reduce lives and notifyObservers
	public void DogDied()
	{

		if(_currentGameState != GameState.GameOver)
		{
			Debug.Log("Dog died");
			_lives--;
			_dogsDrowned++;

			if(_lives == 0)
			{
				GameOver();
			}
			
			NotifyObservers();	
		}
	
	}

	//Buffs & Debuffs
	public void StartScoreMultiplier()
	{
		int multiply = 2;
		int time = 10;		

		StartCoroutine(ScoreMultiplier(multiply, time));
	}

	public void TimeBonus()
	{
		_timer.AddTime(10);
	}

	private IEnumerator ScoreMultiplier(int multiply, int time)
	{
		_scoreMultiplier = multiply;

		NotifyObservers();

		yield return new WaitForSeconds(time);

		_scoreMultiplier = 1;
	}

    public void updateHighScore()
    {
        if (_score > highScore)
        {
			
			highScore = _score;
			//scene = SceneManager.GetActiveScene ();
			PlayerPrefs.SetInt (scene.name, _score);
			//PlayerPrefs.SetInt(highScoreKey, _score);
            PlayerPrefs.Save();
        }
    }

    public int GetHighScore() //get for current scene
    {
		//scene = SceneManager.GetActiveScene ();
		return PlayerPrefs.GetInt(scene.name, 0); 
    }

    public int GetScore()
	{
		return _score;
	}

	public int GetDogsSaved()
	{
		return _dogsSaved;
	}

	public int GetLives()
	{
		return _lives;
	}

	public GameState GetGameState()
	{
		return _currentGameState;
	}

	public int GetScoreMultiplier()
	{
		return _scoreMultiplier;
	}

	//Set the timer and play int
	//TODO - decouple setting and playing of timer- not sure what game will function like
	public void SetTimer(TimerController timer)
	{
		_timer = timer;
		_timer.StartTimer();
	}

	public void GameOver()
	{
		Debug.Log("GAME END");

		//Prevent duplicate calls
		if(_currentGameState != GameState.GameOver)
		{
			_currentGameState = GameState.GameOver;

			//Hide the timer
			GameObject.Find("Timer").SetActive(false);

			//Make all the dogs drown in background because we can

			DrownDogs();


			/*
			Analytics.CustomEvent ("gameOver", new Dictionary<string, object> {
				{ "save", Spawn.saveCount },
				{ "drown", Spawn.drownCount },
				{ "score", _score }
			});
			*/
			if(ac != null && ac.GetComponent<AnalyticsController> () != null)
			{
				ac.GetComponent<AnalyticsController> ().GameOver (_dogsSaved, _dogsDrowned, _score);
			}
		}

	}

	//copied and modified GameOver()
	public void GameWon()
	{
		Debug.Log("GAME WON");

		//Prevent duplicate calls
		if(_currentGameState != GameState.GameWon)
		{
			_currentGameState = GameState.GameWon;
            updateHighScore();

            //Hide the timer
            GameObject.Find("Timer").SetActive(false);

			//"pause" game behind overlay so dogs don't die
			Time.timeScale = 0;
			_timer.StopTimer();
			NotifyObservers();

			if(ac != null && ac.GetComponent<AnalyticsController> () != null)
			{
				//ac.GetComponent<AnalyticsController> ().GameOver (_score);
			}
        }

	}


	private void DrownDogs()
	{
		GameObject[] dogsInScene = GameObject.FindGameObjectsWithTag("Dog");
		Debug.Log("Drown dogs amount: " + dogsInScene.Length);
		foreach(GameObject dog in dogsInScene)
		{
			Debug.Log("Lets kill one");
			var dogController = dog.GetComponent<DogController>();
			if(dogController != null)
			{
				dogController.KillTheDog();
			}

		}
	}

	//Private Because only gamecontroller shoul be adding new dogs...I think
	private void addTODogList(GameObject dog){
		//Just incase
		if (!this._dogsInScene.Contains (dog))
			this._dogsInScene.Add (dog);
	}


	public void removeFromDogList(GameObject dog){
		this._dogsInScene.Remove (dog);
	}

	private IEnumerator SpawnPickUp()
	{
		while(_currentGameState == GameState.Playing)
		{
			
			//Get the boundaries
			BoxCollider bCol = GetComponent<BoxCollider> ();
			
			GameObject pickup = pickUpFactory.Generate();
			Vector3 spawnPoint = pickUpFactory.GenerateSpawn(bCol.bounds.min.z, bCol.bounds.max.z);
			spawnPoint.y = pickup.transform.position.y;
			Instantiate(pickup, spawnPoint,  pickup.transform.rotation);

			float timeToWait = UnityEngine.Random.Range(spawnItemInterval -2, spawnItemInterval + 2);
			yield return new WaitForSeconds(timeToWait);
		}

	}


	// Use to spawn an instant of the dog prefab
	public bool SpawnObjects()
	{
		BoxCollider bCol = GetComponent<BoxCollider> ();

		float z = UnityEngine.Random.Range (bCol.bounds.min.z, bCol.bounds.max.z);
		float xMax = 0.0f;
		float xMin = 0.0f;

		Ray ray = new Ray( new Vector3 (0, 0, z), new Vector3(1,0,0) );
		//TODO Should check if ray intersects insted of assumeing  
		_camFPlanes[1].Raycast (ray, out xMax);
		_camFPlanes[0].Raycast (ray, out xMin);


		xMax = xMax * sideBuffer;
		xMin = xMin * sideBuffer;
			
		float x = UnityEngine.Random.Range (xMin, xMax);

		spawnChecker.transform.position = new Vector3 (x, 0, z);

		bool spawnLocationCovered = false;
		foreach(GameObject dog in this._dogsInScene){
			if (spawnChecker.GetComponent<Collider> ().bounds.Intersects (dog.GetComponent<Collider> ().bounds)) {
				spawnLocationCovered = true;
				break;
			}
							
		}

		if(!spawnLocationCovered){
			foreach(GameObject obstacle in obstacles){
				if (spawnChecker.GetComponent<Collider> ().bounds.Intersects (obstacle.GetComponent<Collider> ().bounds)) {
					spawnLocationCovered = true;
					break;
				}
			}
		}

		if (!spawnLocationCovered) {
			Vector3 spawnLocation = new Vector3 (x, -3f, z);
			GameObject selectDog = dogPrefabs.PickRandom (); // Initialising separately so I can get the rotation for spawn (mimi)
			GameObject dog = (GameObject)Instantiate (selectDog, spawnLocation, selectDog.transform.rotation) ;
			//dog.transform.parent = transform;
			addTODogList(dog);
			return true;
		}

		return false;
	}


	public void RubbishPickedUp(Vector3 pickUpPosition)
	{
		//Will this be different for different levels?
		_score -= 500;

		if(_score < 0) _score = 0;

		//Show text popup
        var position = Camera.main.WorldToScreenPoint(pickUpPosition);
        TextPopUpManager.Instance.CreateText("-500", position, 1f, 0.2f, 0.2f);

		NotifyObservers();
	}

	public void RubberRingPickedUp()
	{
		//Go through and add rings to the dogs
		GameObject[] dogsInScene = GameObject.FindGameObjectsWithTag("Dog");

		var ring = Resources.Load("Ring") as GameObject;

		foreach(GameObject dog in dogsInScene)
		{
			var dogController = dog.GetComponent<DogController>();
			if (dogController != null) {
				if (dogController.currentState == dogController.floatingState || dogController.currentState == dogController.drowningState) {
					Vector3 ringSpawn = dog.transform.position;
					ringSpawn.y = ring.transform.position.y;
					var createdRing = Instantiate(ring, ringSpawn,  Quaternion.identity) as GameObject;
					createdRing.transform.SetParent(dog.transform);
					dogController.SetRingTimeout();
				
				}
					
			}

		}

	}
}
