﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Analytics;

public class AnalyticsController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GameOver (int saveCount, int drownCount, int score){
		Analytics.CustomEvent("gameOver", new Dictionary<string, object> {
			{ "save", saveCount },
			{ "drown", drownCount },
			{ "score", score }
		});
	}
}