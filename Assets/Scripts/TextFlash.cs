﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextFlash : MonoBehaviour {

	public Text textString;
	public GameObject canvas;

	private Vector3 position;
	private RectTransform rectTransform;

	private bool _fadingIn;
	private bool _fadingOut;
	private float _lastFadeAlpha = 0;
	private float speed = 0.05f;

	private float r = 1f, g = 1f, b = 1f; 

	// Use this for initialization
	void Start () 
	{
		_fadingIn = true;

		transform.SetParent(GameObject.Find("Canvas").transform, false);
		//textString.transform.position = Vector3.zero;
		rectTransform = textString.GetComponent<RectTransform>();
		rectTransform.anchoredPosition = position;
		textString.transform.position = position;
		rectTransform.localScale = new Vector3(1,1,1);
	}

	// Update is called once per frame
	void Update () 
	{
		if(_fadingIn)
		{
			//fade the alpha in
			_lastFadeAlpha = Mathf.Lerp(_lastFadeAlpha, 1f, Constants.timeScale * speed);
			textString.color = new Color(r, g, b, _lastFadeAlpha);

			//if reached certain point
			if(_lastFadeAlpha > 0.97f)
			{
				_fadingIn = false;

				StartCoroutine(ShowThenFade());
			}
				//start 2 second coroutine that then calls fadeOut
		}

		if(_fadingOut)
		{
			//fade the alpha in
			_lastFadeAlpha = Mathf.Lerp(_lastFadeAlpha, 0, Constants.timeScale * speed);
			textString.color = new Color(r, g, b, _lastFadeAlpha);

			//if reached certain point
			if(_lastFadeAlpha < 0.1f)
			{
				Destroy(gameObject);
			}
		}
	}

	private IEnumerator ShowThenFade()
	{
		//Using the class located at bottom of HelpController - bad programming practice? Who cares last week
		yield return StartCoroutine(CoroutineUtil.WaitForRealSeconds(1));
		_fadingOut = true;
	}

	//Once the text is set it will fade in
	//RGB refer to the colour values
	public void SetText(string text, Vector3 textPostition, float rVal, float gVal, float bVal)
	{
		textString.text = text;
		position = textPostition;
		r = rVal;
		g = gVal;
		b = bVal;
		//textString.transform.position = Camera.main.WorldToViewportPoint(position);

	}

}
