﻿using UnityEngine;
using System.Collections;

public class FishingLine : MonoBehaviour {

	private LineRenderer lineRenderer;
	private GameObject lure;

	// Use this for initialization
	void Start () {
		lineRenderer = GetComponent<LineRenderer>();
		lure = GameObject.Find ("Lure");
		lineRenderer.SetVertexCount(2);
	}
	
	// Update is called once per frame
	void Update () {
		lineRenderer.SetPosition(0, transform.position);
		lineRenderer.SetPosition(1, lure.transform.position);
	}
}
