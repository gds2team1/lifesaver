﻿using UnityEngine;
using System.Collections;

public class RodAnimator : MonoBehaviour {

	//public bool pull;
	//public bool cast;
	//public bool reel;

	private Animator anim;

	void Awake()
	{
		anim = GetComponent<Animator> ();
	}

	public void Pull()
	{
		anim.SetBool ("pull", true);
		anim.SetBool ("cast", false);
		anim.SetBool ("reel", false);
	}

	public void Cast()
	{
		anim.SetBool ("pull", false);
		anim.SetBool ("cast", true);
		anim.SetBool ("reel", false);

	}

	public void Reel()
	{
		anim.SetBool ("pull", false);
		anim.SetBool ("cast", false);
		anim.SetBool ("reel", true);
	}

	public void Stop()
	{
		anim.SetBool ("reel", false);
	}
}
