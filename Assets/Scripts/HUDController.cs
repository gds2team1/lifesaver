﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class HUDController : MonoBehaviour, IGameObserver  {

	public Text scoreText;
	public Text scoreTextStroke;
	public GameObject livesHUD;
	public GameObject gameHUD;
	public GameObject pausePanel; 
	public GameObject gameOverPanel; 
	public GameObject gameWinPanel; 
	public GameObject pauseButton; 
	public GameObject settingsPanel;

    public Text finalScore;
	public Text dogsSaved;

    public Text highScoreWin;
    public Text finalScoreWin;
	public Text dogsSavedWin;

	private GameController _gameInstance;

	private TimerController _timer;


	private bool _isMultiplying = false;

	//Fading-Overlay Stuff
	public GameObject overlay;
	private SpriteRenderer _overlaySprite;
	private GameObject _incomingPanel;
	private bool _isPanelComingIn = false;
	private bool _isFadingIn = false;
	private bool fadingOverlay = false;
	private float _lastFadeAlpha;
	//Start the directon going down
	private float _panelDirection;
	//Where we want panels to animate/drop down to
	public Vector3 _panelPosition;
	private RectTransform _panelRectTransform;
	private float _previousTimeScale = 1;

	private void Start()
	{
		Debug.Log("A HUD controller was created");

		_gameInstance = GameController.GetInstance();
		_gameInstance.AddObserver(this);

		//scoreTextStroke = scoreText.GetComponentInChildren<Text> ();
		scoreText.text = "0";
		scoreTextStroke.text = "0";

		pausePanel.SetActive(false); //Katie UI
		gameOverPanel.SetActive(false); //Katie UI
		gameWinPanel.SetActive(false); //Katie UI
		settingsPanel.SetActive(false);

		_isPanelComingIn = false;
		_isFadingIn = false;
		fadingOverlay = false;

		_overlaySprite = overlay.GetComponent<SpriteRenderer>();
		_previousTimeScale = Time.timeScale;
		_panelDirection = -1;

		_timer = GameObject.Find("Timer").GetComponent<TimerController>();

	}

	private void Update() { 
		//blank 
		if(_isFadingIn)
		{
			FadeInOverlay();
		}

		if(_isPanelComingIn)
		{
			AnimatePanelIn();
		}

		if(_isMultiplying)
		{
			FlashScore();
		}

		//Magic key to make it happen
		if(Input.GetKey("r"))
		{
			ShowUI(gameOverPanel);
		}
	}

	void ShowOverlay()
	{
		pauseButton.SetActive(false);
		//scoreText.SetActive(false);
		livesHUD.SetActive(false);
		gameHUD.SetActive(false);
		//somehow hide tbe UI score
		_isFadingIn = true;
		//Fade in background
		Debug.Log("Overlay active");
		overlay.SetActive(true);
	}

	void FadeInOverlay()
	{
		float fadeInTime = 0.1f;

		_lastFadeAlpha = Mathf.Lerp(_lastFadeAlpha, 0.9f, _previousTimeScale * fadeInTime);
		_overlaySprite.color = new Color(1f, 1f, 1f, _lastFadeAlpha);



		//Exit if reached certain Fade - and move fade in
		if(_lastFadeAlpha > 0.8f)
		{
			_isFadingIn = false;

			//Change the panel we want to come in here
			BringPanelIn();
		}
	}

	void BringPanelIn()
	{	

		//Change panel position to above the screen
		_incomingPanel.transform.position = new Vector2(_incomingPanel.transform.position.x, _incomingPanel.transform.position.y + 200);

		_isPanelComingIn = true;
		_incomingPanel.SetActive(true);
	}

	void ShowUI(GameObject panel)
	{

		_panelPosition = panel.transform.position;

		_incomingPanel = panel;

		//Trigger sequence of events
		ShowOverlay();
	}

	void AnimatePanelIn()
	{
		float dropDownSpeed = 0.1f;

		Vector2 oldPos = _incomingPanel.transform.position;
		float newY = Mathf.Lerp(oldPos.y, _panelPosition.y, dropDownSpeed * _previousTimeScale);
		_incomingPanel.transform.position = new Vector2(oldPos.x, newY);

		//Panel has fallen below limit
		if(oldPos.y <= _panelPosition.y + 1)
		{
			_isPanelComingIn = false;
		}
	}

	private void ChangeScore(int score)
	{
		string text = _isMultiplying ?  score.ToString() + " x2" : score.ToString();
		scoreText.text = text;
		scoreTextStroke.text = text;

	}

	//Go through and disable dog sprites based on amount dead
	private void ChangeLives(int lives)
	{

		if(lives >= 0)
		{

			int dead = Constants.lives - lives;
			for(int i = 0; i < dead; i++ )
			{
				if (livesHUD.transform.childCount >= i) {
					//GameObject life = livesHUD.transform.GetChild (i).gameObject;
					//BubbleAnimator anim = life.GetComponentInChildren<BubbleAnimator> ();
					//anim.Pop ();
					livesHUD.transform.GetChild (i).GetComponent<BubbleAnimator> ().Pop ();
					//livesHUD.transform.GetChild (i).gameObject.SetActive(false);
					//life.SetActive(false);
				}
			}
		}

		
	}

	public void UpdateGameStats()
	{
		Debug.Log("HUD got update");
		int score = _gameInstance.GetScore();
		int lives = _gameInstance.GetLives();

		int multiplier = _gameInstance.GetScoreMultiplier();
		if(multiplier != 1)
		{
			FlashMultiplier(multiplier);
		}

		ChangeScore(score);
		ChangeLives(lives);

		if(_gameInstance.GetGameState() == GameState.GameOver)
		{
			ShowUI(gameOverPanel);
			UpdateGameOverScore();
		}
		else if(_gameInstance.GetGameState() == GameState.GameWon)
		{
			ShowUI(gameWinPanel);
			UpdateGameWonScore();
		}
		else if(_gameInstance.GetGameState() == GameState.Paused)
		{
			Debug.Log("Game is now paused");
			ShowUI(pausePanel);
		}
		else if(_gameInstance.GetGameState() == GameState.Playing)
		{
			pauseButton.SetActive(true);
			pausePanel.SetActive(false);
			overlay.SetActive(false);
			livesHUD.SetActive(true);

			//Timer has to be active element to start
			gameHUD.SetActive(true);
			_timer.StartTimer();


		}
	}

	private void UpdateGameWonScore()
	{
        highScoreWin.text = "High score: " + _gameInstance.GetHighScore().ToString();
        //Change score
        finalScoreWin.text = "Final score: " + _gameInstance.GetScore().ToString();
		//Change dogs saved
		dogsSavedWin.text = "Dogs saved: " + _gameInstance.GetDogsSaved().ToString();
	}

	private void UpdateGameOverScore()
	{
		//Change score
		finalScore.text = "Final score: " + _gameInstance.GetScore().ToString();
		//Change dogs saved
		dogsSaved.text = "Dogs saved: " + _gameInstance.GetDogsSaved().ToString();
	}

	//Katie UI
	public void togglePausePanel(GameObject panel)
	{
		panel.SetActive(!panel.activeSelf);
	}

	public void restartLevel()
	{
		//SceneManager.LoadScene("Main");
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	// Relies on level to be in the right buld order > Build Settings
	public void Nextlevel()
	{
		Debug.Log("Deprecated use Scene Controller");
		
		int index = SceneManager.GetActiveScene().buildIndex;
		index++;
		SceneManager.LoadScene(sceneBuildIndex:index);
	}

	private void quitLevel()
	{
		Debug.Log("Deprecated use Scene Controller");
		SceneManager.LoadScene("TitleScreen");
	}

	public void showSettings()
	{
		pausePanel.SetActive(false);
		ShowUI(settingsPanel);
	}

	public void closeSettings()
	{
		settingsPanel.SetActive(false);
		ShowUI(pausePanel);
	}


     public void FlashMultiplier(int multiplier)
     {
     	_isMultiplying = true;
        StartCoroutine(RemoveMultiplier());
     }

     //Don't get angry at the hard code I got a bit lazy :(
     private IEnumerator RemoveMultiplier()
     {
        yield return new WaitForSeconds(10);
 		scoreTextStroke.color = Color.white;

 		ChangeScore(_gameInstance.GetScore());
        _isMultiplying = false;
        
     }

     private void FlashScore()
     {
     	float speed = 2;
     	float pingPong = Mathf.Round(Mathf.PingPong(Time.time * speed, 1.0f));

     	if(pingPong == 0)
     	{
     		scoreTextStroke.color = Color.yellow;
     	}
     	else
     	{
     		scoreTextStroke.color = Color.white;
     	}
     }


			
}
