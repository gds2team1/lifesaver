﻿using UnityEngine;
using System.Collections;

public interface IGameObserver{

	void UpdateGameStats();
}
