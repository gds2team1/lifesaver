﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlingShotController : MonoBehaviour
{

	public GameObject springMount;
	public GameObject movementPlaneObject;
	public GameObject camera;

	public AudioClip reelInSound;
	public AudioClip chime;
	public AudioClip debuff;

	public GameObject rodObject;
	private RodAnimator rod;

	private SpringJoint spring;
	private LineRenderer lineRenderer;

	private Vector3 oldMousePos;
	private float distanceToCam;
	private Vector3 prevVelocity;

	private Rigidbody rigidbody;

	private Plane movementPlane;

	private bool dragging = false;

	private Vector3 startPosition;
	private Vector3 dragTo = new Vector3(0, 2f, -8.6f);
	private Vector3 dragFrom = Vector3.zero;
	private bool reelingIn = false;

	public float reelSpeed = 1;

	public Material seaweedMat;
	private Material[] materials;

	private GameController _gameInstance;
	private GameState previousGameState;
	public GameObject lastInTheConga;

	void Awake ()
	{
		spring = GetComponent <SpringJoint> ();
		rigidbody = GetComponent<Rigidbody> ();
		lineRenderer = GetComponent<LineRenderer> ();
		if (rodObject == null) {
			rodObject = GameObject.Find ("rod");
		}
		rod = rodObject.GetComponent<RodAnimator> ();

		rigidbody.isKinematic = true;

		_gameInstance = GameController.GetInstance ();

	}

	// Use this for initialization
	void Start ()
	{
		distanceToCam = Vector3.Distance (camera.GetComponent<Transform> ().position, transform.position);
		movementPlane = new Plane (movementPlaneObject.transform.up, movementPlaneObject.transform.position);

		startPosition = transform.position;
		lastInTheConga = this.gameObject;

		previousGameState = GameState.Playing;
	}

	// Update is called once per frame
	void Update ()
	{
		//Debug.Log (dragging);
		if (spring != null) {
			if (!rigidbody.isKinematic && prevVelocity.sqrMagnitude > rigidbody.velocity.sqrMagnitude) {
				Destroy (spring);
				rigidbody.velocity = prevVelocity;
			}
			if (!dragging)
				prevVelocity = rigidbody.velocity;
			else
				DrawTrajectory ();
		}
		if (reelingIn) {
			ReelIn ();
		}

		if (transform.position.y <= Constants.waterLevel) {
			HitWater ();
		}

		if (Input.GetKeyDown("w")) //simulate seaweed
		{
			DecreaseSpeed ();
		}

		// Pause the reeling sound while paused and unpause it when resuming
		if (GameController.GetInstance ().GetGameState () == GameState.Paused && previousGameState == GameState.Playing) {
			GetComponent<AudioSource> ().Pause ();
			previousGameState = GameState.Paused;
		} else if (GameController.GetInstance ().GetGameState () == GameState.Playing && previousGameState == GameState.Paused) {
			GetComponent<AudioSource> ().UnPause ();
			previousGameState = GameState.Playing;
		}
	}

	void OnMouseDown ()
	{
		if (GameController.GetInstance ().GetGameState () == GameState.Playing) {
			rod.Pull ();
			dragging = true;
			Vector3 mousePos = Input.mousePosition;
			mousePos.z = distanceToCam;
			oldMousePos = Camera.main.ScreenToWorldPoint (mousePos);
		}
	}

	void OnMouseUp ()
	{
		if (GameController.GetInstance ().GetGameState () == GameState.Playing) {
			rod.Cast ();
			dragging = false;
			rigidbody.isKinematic = false;

			//Remove tutorial Lines
			lineRenderer.SetVertexCount (0);
			lineRenderer = GetComponent<LineRenderer> ();
		}
	}

	void OnMouseDrag ()
	{
		if (GameController.GetInstance ().GetGameState () == GameState.Playing) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			float distance;
			if (movementPlane.Raycast (ray, out distance)) {
				// restrict y and z possible drag positions
				Vector3 dragPos = ray.GetPoint (distance);
				dragPos.y = Mathf.Clamp (dragPos.y, -10f, 1.14f);
				dragPos.z = Mathf.Clamp (dragPos.z, -40f, -8.55f);
				transform.position = dragPos;
			}
		}
	}

	//TODO - calculate spring on the fly - a bit hard coded
	void DrawTrajectory ()
	{
		if(lineRenderer != null)
		{
			float springForce = spring.spring / 1000;

			Vector3 springVelocity = new Vector3 (24 * springForce, 14f * springForce, 22f * springForce);
			Vector3 directionVector = startPosition - transform.position;
			Vector3 trajectoryVelocity = Vector3.Scale (springVelocity, directionVector);
			PlotTrajectory (transform.position, trajectoryVelocity, 0.05f, 2f);
		}


	}

	//http://answers.unity3d.com/questions/296749/display-arc-for-cannons-ball-trajectory.html
	void PlotTrajectory (Vector3 start, Vector3 startVelocity, float timestep, float maxTime)
	{

		Vector3 prev = start;

		lineRenderer.SetVertexCount (0);
		//lineRenderer.sortingLayerName = "OnTop";
		//lineRenderer.sortingOrder = 5;
		//lineRenderer.SetPosition(1, new Vector3(5, 4, 5));

		List<Vector3> points = new List<Vector3> ();

		for (int i = 1;; i++) {
			float t = timestep * i;
			if (t > maxTime)
				break;
			Vector3 pos = PlotTrajectoryAtTime (start, startVelocity, t);


			if (Physics.Linecast (prev, pos)) {
				//Move red ring to this position

				break;
			}

			points.Add (pos);

			//Debug.DrawLine (prev,pos,Color.red);
			prev = pos;
		}

		lineRenderer.SetVertexCount (points.Count);

		lineRenderer.SetPosition (0, transform.position);

		for (var i = 1; i < points.Count; i++) {
			lineRenderer.SetPosition (i, points [i]);
		}

		lineRenderer.SetWidth (0.1f, 0.1f);
		lineRenderer.useWorldSpace = true;
	}

	Vector3 PlotTrajectoryAtTime (Vector3 start, Vector3 startVelocity, float time)
	{
		return start + startVelocity * time + Physics.gravity * time * time * 0.5f;
	}

	private void HitWater ()
	{
		if (!reelingIn){
			//Stop the dog
			rigidbody.velocity = Vector3.zero;

			//Lock the y
			rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX
				| RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

			dragFrom = transform.position;
			// sound effect should play just once and not stack when it runs into every collider
			GetComponent<AudioSource> ().PlayOneShot (reelInSound);
			reelingIn = true;
		}
	}

	void OnCollisionEnter (Collision collision)
	{
		if (collision.gameObject.tag.Equals ("Water")) {
			HitWater();
		}

		/*
		if (collision.gameObject.tag.Equals ("Dog")) {

			//Remove gravity - add buoyoncy
			rigidbody.velocity = Vector3.zero;
			rigidbody.useGravity = false;

			//Lock the y
			rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX
				| RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			//Attach the dog to the 'bait'
			collision.gameObject.transform.SetParent(transform);

			//Let dog know it is being reeled in
			collision.gameObject.GetComponent<DogController> ().Caught ();

			dragFrom = transform.position;

			reelingIn = true;
			GetComponent<AudioSource> ().PlayOneShot (reelInSound);
		}
		*/

		else if(collision.gameObject.tag.Equals ("Pickup"))
		{
			GameObject pickup = collision.gameObject;
			pickup.GetComponent<IPickUp> ().PickedUp ();
			switch (pickup.name) {
				case "Rubbish(Clone)":
				case "Seaweed(Clone)":
					GetComponent<AudioSource> ().PlayOneShot (debuff);
					break;
				default:
					GetComponent<AudioSource> ().PlayOneShot (chime);
					break;
			}

			// force lure to go straight down to prevent the crazy bouncing when hitting a lure
			this.rigidbody.velocity = new Vector3(0, -1, 0);
			//Add to conga line?
			Destroy(collision.gameObject);
		}
	}

	public void dogCaught(GameObject dog)
	{
		lastInTheConga = dog;
	}

	IEnumerator WaitForReel ()
	{
		yield return new WaitForSeconds (2);

		reelingIn = true;
		dragFrom = transform.position;
	}

	void ReelIn ()
	{
		rod.Reel();


		//Keep the dog/bait on the water when reeling in

		if(dragFrom != Vector3.zero)
		{
			dragTo.y = dragFrom.y;
		}

		//Drag to y should be the differ
		//var direction = dragTo - dragFrom;

		//rigidbody.velocity = direction * reelSpeed;

		Vector3 platformPos = new Vector3 (0, 0, Constants.platformZ);

		transform.position = Vector3.MoveTowards (transform.position, platformPos, Time.deltaTime * -1 * Constants.DOG_SWIM_SPEED * reelSpeed);

		//Dog has been rescued
		if (transform.position.z <= Constants.platformZ + 0.5f) {
			reelingIn = false;
            GetComponent<AudioSource>().Stop();//reel sound
			if (lastInTheConga != this.gameObject) {
				GetComponent<AudioSource> ().PlayOneShot (chime);
			}
			Reset ();
		}
	}

	void Reset ()
	{
		lastInTheConga = this.gameObject;
		transform.position = startPosition;

		dragFrom = Vector3.zero;

		rigidbody.isKinematic = true;
		rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
		rigidbody.useGravity = true;

		//Reset the spring
		if (GetComponent<SpringJoint> () == null) {
			gameObject.AddComponent<SpringJoint> ();
		}

		spring = GetComponent<SpringJoint> ();

		spring.connectedBody = springMount.GetComponent<Rigidbody> ();
		spring.anchor = Vector3.zero;
		spring.connectedAnchor = Vector3.zero;
		spring.spring = 1200;

	}

	public void IncreaseSpeed()
	{
		reelSpeed *= 3;
		StartCoroutine(ResetSpeed());
	}

	public void DecreaseSpeed()
	{
		reelSpeed /= 2;
		materials = GetComponent<Renderer> ().materials;
		materials [1] = seaweedMat;
		GetComponent<Renderer> ().materials = materials;

		StartCoroutine(ResetSpeed());
	}

	public bool isReeling()
	{
		return reelingIn;
	}

	//Wait x seconds for powerup to cool down
	private IEnumerator ResetSpeed()
	{
		yield return new WaitForSeconds(10);
		materials = GetComponent<Renderer> ().materials;
		materials [1] = null;
		GetComponent<Renderer> ().materials = materials;

		reelSpeed = 1;
	}

}
