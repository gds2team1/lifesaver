﻿using UnityEngine;
using System.Collections;

public class CreditScroller : MonoBehaviour {
	private Vector3 direction = new Vector3(0,1,0);
	Vector3 textPos;

	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if (this.gameObject.transform.localPosition.y < 1600f)
		this.gameObject.transform.position += direction * 200f * Time.fixedDeltaTime;
	}


}
