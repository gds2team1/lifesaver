﻿using UnityEngine;
using System.Collections;

public static class RandomUtil{

	private static System.Random rand = null;

	/*Still random but weighted choices */
	public static T PickRandom<T>(this T[] values)
	{
		if (rand == null)
		{
			rand = new System.Random ();
		}

		float randomValue = rand.Next (0, 100);

		if(randomValue < 40)
		{
			return values[0];
		}
		if(randomValue < 70)
		{
			return values[1];
		}
		if(randomValue < 90)
		{
			return values[2];
		}

		//Return the mighty rare shiba
		return values[3];
	}
}
