﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
 
 //Modified the code from http://answers.unity3d.com/questions/980339/count-down-timer-c-1.html
 public class TimerController: MonoBehaviour {
 
     public float timeLeft = 300.0f;
     public bool stop = true;
 
     private float minutes;
     private float seconds;
     
     public Text text;
     private GameController _gameInstance;
	private SlingShotController lure;

     void Start()
     {
		lure = GameObject.Find ("Lure").GetComponent<SlingShotController> ();
		_gameInstance = GameController.GetInstance();
     	_gameInstance.SetTimer(this);
     }

     public void StartTimer()
     {
         stop = false;
         StartCoroutine(UpdateTime());
     }

     public void ResumeTimer()
     {
        stop = false;
     }

     public void StopTimer()
     {
     	stop = true;
     }

     public void AddTime(int time)
     {
        Debug.Log("Add time from timer controller");

        Vector3 position = text.transform.position;//GetComponent<RectTransform>().anchoredPosition;
        position.y -= 40;
		Instantiate (Resources.Load("Stars") as GameObject, position , Quaternion.identity);
		TextPopUpManager.Instance.CreateText("+" + time + " sec", position, 0.46f, 0.35f, 0.27f);
		TextPopUpManager.Instance.CreateText("+" + time + " sec", new Vector3(position.x -1, position.y +1, position.z), 1f, 1f, 1f);

        timeLeft += time;
     }

     
     void Update() 
     {
     	//if paused end here
         if(stop)
        {
         	return;
        }

        timeLeft -= Time.deltaTime;
         
        //Convert time into seconds and minutes
        minutes = Mathf.Floor(timeLeft / 60);
        seconds = timeLeft % 60;
        if(seconds > 59) seconds = 59;
        if(minutes < 0) {
            stop = true;
            minutes = 0;
            seconds = 0;
        }

        if(timeLeft <= 0)
        {
        	StopTimer();

        	//Wouldnt actually do this
        	//text.text = "Game over";
			if (!lure.isReeling ()) {
				_gameInstance.GameWon ();
			}
        }
     }
 
     private IEnumerator UpdateTime()
     {
     	//While running update the time - check every 0.2 seconds = better performance for interacting with UI
         while(!stop)
         {
             text.text = string.Format("{0:0}:{1:00}", minutes, seconds);
             yield return new WaitForSeconds(0.2f);
         }
     }
 }