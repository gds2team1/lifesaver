﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TrajectoryAnimation : MonoBehaviour {
    public float scrollSpeed = 0.5F;
    private Renderer rend;
	private TimerController timer;
	public bool showHelp;
	public float helpCutOffTime;

    void Start() {
		timer = GameObject.Find ("Timer").GetComponent<TimerController> ();
		rend = GetComponent<LineRenderer> ();
		if (!showHelp) {
			rend.enabled = false;
		}
			
    }
    void Update() 
	{
		if (showHelp) {
			float offset = Time.time * scrollSpeed;
			rend.material.SetTextureOffset ("_MainTex", new Vector2 (-offset, 0));

			if (timer.timeLeft <= helpCutOffTime && SceneManager.GetActiveScene().name != "Level1") {
				showHelp = false;
				rend.enabled = false;
			}
		}
    }
}