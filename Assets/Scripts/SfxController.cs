﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SfxController : MonoBehaviour {

	public bool soundOn;

	void Start () 
	{
		// 0 = on, 1 = off
		if (PlayerPrefs.GetInt ("SfxMute") == 0) {
			GetComponent<Toggle> ().isOn = true;
			soundOn = true;
			AudioListener.pause = false;
		} else {
			GetComponent<Toggle> ().isOn = false;
			soundOn = false;
			AudioListener.pause = true;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void toggleMuteSfx()
	{
		if (soundOn) //turn off
		{
			soundOn = false;
			AudioListener.pause = true;
			PlayerPrefs.SetInt ("SfxMute", 1);
		} 
		else //turn on
		{
			soundOn = true;
			AudioListener.pause = false;
			PlayerPrefs.SetInt ("SfxMute", 0);
		}
	}
}
