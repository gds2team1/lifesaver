﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour{

	//Gravity
	public static float gravityStrength = 9.8f;
	//public static float gravityRadius = 1.0f;
	public static float dogRadius = 0.2f;
	public static float timeScale = 1.0f;


	

	public static float platformZ = -8.5f;
	public static float waterLevel = 0.1f;
	public static int lives = 3;

	public static float DOG_SWIM_SPEED = -4.0f;
	public static float DOG_TURN_SPEED = 18.5f;

}
