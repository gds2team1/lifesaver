﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PickUpFactory{

	//private List<GameObject> pickUps = new List<GameObject>();
	private int level;
	private int timeInterval;
	private bool stop = false;
	private Plane[] _camFPlanes;
	public float sideBuffer = 0.5f;
	private string currentLevel;

	//Must map to the names of pickups in resources folder
	private string[] pickUps = {"Beachball", "Sunscreen", "RingPickUp", "Rubbish", "Seaweed", "Taiyaki", };

	public PickUpFactory(int level)
	{
		Debug.Log("Initialised factory");
		this.level = level;

	}

	public void Start()
	{
		_camFPlanes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
		currentLevel = SceneManager.GetActiveScene ().name;
	}

	public void Update() { 
		//blank
	}

	public GameObject Generate()
	{
		
		GameObject item = GetItem();
		return item;
			
	}

	public Vector3 GenerateSpawn(float zMin, float zMax)
	{
			
		//Lets assume that both sides are symmetrical so only get left side
		Plane plane = new Plane(Vector3.up, new Vector3(0,0,0));
	   	Ray ray;
	    float distance;
	    Vector3 botLeft = new Vector3();
	    Vector3 topLeft = new Vector3();

	    ray = Camera.main.ViewportPointToRay(new Vector3(0,0,0)); // bottom left ray
	    if (plane.Raycast(ray, out distance)){
	     botLeft  = ray.GetPoint(distance);
	    }

	    ray = Camera.main.ViewportPointToRay(new Vector3(0,1,0)); // top left ray
	    if (plane.Raycast(ray, out distance)){
	      topLeft  = ray.GetPoint(distance);
	    }

	    //Restrict z to certain bounds?
		float z = UnityEngine.Random.Range (zMin, zMax);

	   	//Wooh year 10's math 
	    float gradient = (topLeft.z - botLeft.z) / (topLeft.x - botLeft.x);
	    //Constant = z - mx
	    float constant = topLeft.z - (gradient *topLeft.x);

	    //Adams calculations of line formulate x = (z - constant)/gradient(m)	    
	    float x = (z - constant)/gradient;
		
	    //Randomly determine if coming in from right or left
		bool onLeft = UnityEngine.Random.Range (-1, 1) < 0;
		if(onLeft)
		{
			//Spawn just left off the screen
			return new Vector3(x, 0f, z);
		}
		else
		{
			//Spawn just right off the screen
			return new Vector3(x * -1, 0f, z);
		}

	}

	//Based on the level 
	private GameObject GetItem()
	{
		Random random = new Random();
		int randomIndex = 0;

		switch (SceneManager.GetActiveScene ().name) {
		case "Level2":
			randomIndex = Random.Range (0, 2); //suncreen + beach ball
			break;
		case "Level3":
			randomIndex = Random.Range (0, 4); //suncreen, beach ball, rubber ring, rubbish
			break;
		case "Level4":
		case "Level5":
			randomIndex = Random.Range (0, pickUps.Length); //suncreen, beach ball, rubber ring, rubbish
			break;
		default: 
			randomIndex = Random.Range (0, pickUps.Length); //all
			break;
		}

		string item = pickUps [randomIndex];
		return Resources.Load (item) as GameObject;

	}
		// Ye olde way of spawning
		/*
		Random random = new Random();
		int randomIndex = Random.Range(0, pickUps.Length);
		string item = pickUps[randomIndex];
		Debug.Log("Spawn item: " + item);
		return Resources.Load(item) as GameObject;
		*/

}