﻿using UnityEngine;
using System.Collections;

public class RubbishPickUp : PickUpBase, IPickUp {

	public void PickedUp()
	{
		Debug.Log("Rubbish was just picked up");
		//Add time
		GameController.GetInstance().RubbishPickedUp(transform.position);
	}


}
