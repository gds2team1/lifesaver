﻿using UnityEngine;
using System.Collections;

public class RubberRingPickUp : PickUpBase, IPickUp {

	public void PickedUp()
	{
		Debug.Log("Rubber Ring Picked up was just picked up");
		var position = Camera.main.WorldToScreenPoint(this.transform.position);
		Instantiate (Resources.Load("Stars") as GameObject, position, Quaternion.identity);	
		//Add time

		GameController.GetInstance().RubberRingPickedUp();
	}


}
