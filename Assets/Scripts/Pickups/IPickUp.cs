﻿using UnityEngine;
using System.Collections;

public interface IPickUp
{
	void PickedUp();
	void SetDirection(int direction);
}

