﻿using UnityEngine;
using System.Collections;

public class SeaweedPickUp : PickUpBase, IPickUp {

	public void PickedUp()
	{
		var position = Camera.main.WorldToScreenPoint(this.transform.position);
		TextPopUpManager.Instance.CreateText("Slow...", position, 1f, 1f, 1f);
		//Change the speed
		var lure = GameObject.Find("Lure").GetComponent<SlingShotController>();
		lure.DecreaseSpeed();
	}


}
