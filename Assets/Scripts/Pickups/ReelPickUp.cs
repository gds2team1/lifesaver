﻿using UnityEngine;
using System.Collections;

public class ReelPickUp : PickUpBase, IPickUp {

	public void PickedUp()
	{
		var position = Camera.main.WorldToScreenPoint(this.transform.position);
		Instantiate (Resources.Load("Stars") as GameObject, this.transform.position, Quaternion.identity);
		TextPopUpManager.Instance.CreateText("Speed boost!", position, 1f, 1f, 1f);
		//Get the lure

		//Change the speed
		var lure = GameObject.Find("Lure").GetComponent<SlingShotController>();
		lure.IncreaseSpeed();
	}


}
