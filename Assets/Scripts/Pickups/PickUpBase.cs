﻿using UnityEngine;
using System.Collections;

public class PickUpBase : MonoBehaviour, IPickUp {

	private Rigidbody rb;
	private float speed = 1;
	private int direction;
	private bool isGood;

	// Use this for initialization
	void Start () 
	{

		rb = GetComponent<Rigidbody>();
		direction = transform.position.x > 0 ? -1 : 1;
		//rb.AddForce(direction * speed, 0, 0);
	}
	
	// Update is called once per frame
	void Update () 
	{
		rb.velocity = new Vector3(direction * speed, 0, 0);
		//Better performance to calculate when these are off edge of viewport
		if(transform.position.x < -10 || transform.position.x > 10)
		{
			Destroy(gameObject);
		}
	}

	public void PickedUp()
	{
	//override
	}

	public void SetDirection(int direction)
	{
		Debug.Log("set the results: " + direction);

	}
}
