﻿using UnityEngine;
using System.Collections;

public class ScorePickUp : PickUpBase, IPickUp {

	public void PickedUp()
	{
		Debug.Log("Score was just picked up");
		//Add time
		var position = Camera.main.WorldToScreenPoint(this.transform.position);
		TextPopUpManager.Instance.CreateText("Score x2!", position, 1f, 1f, 1f);
		Instantiate (Resources.Load("Stars") as GameObject, this.transform.position, Quaternion.identity);
		GameController.GetInstance().StartScoreMultiplier();
	}


}
