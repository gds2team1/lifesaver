﻿using UnityEngine;
using System.Collections;

public class TimePickUp : PickUpBase, IPickUp {

	public void PickedUp()
	{
		Debug.Log("Time was just picked up");
		var position = Camera.main.WorldToScreenPoint(this.transform.position);
		Instantiate (Resources.Load("Stars") as GameObject, position, Quaternion.identity);
		//Add time
		GameController.GetInstance().TimeBonus();
	}


}
